# Prevent brp-python-bytecompile from running.
%define __os_install_post %{___build_post}

# "Harbour RPM packages should not provide anything."
%define __provides_exclude_from ^%{_datadir}/.*$

Name: harbour-mqttea
Version: 0.0.0
Release: jolla
Summary: Application to handle hash digests
License: GPLv3+
URL: https://gitlab.com/nobodyinperson/harbour-mqttea
Source: %{name}-%{version}.tar.xz
Packager: Yann Büchau <nobodyinperson@posteo.de>
Conflicts: %{name}
BuildArch: noarch
BuildRequires: gettext
BuildRequires: inkscape
BuildRequires: gettext
BuildRequires: pandoc
BuildRequires: make
Requires: pyotherside-qml-plugin-python3-qt5 >= 1.4
Requires: libsailfishapp-launcher
Requires: python3-base
Requires: sailfishsilica-qt5

%description
Handle hash digests

%prep
%setup -q

%install
./configure --prefix=/usr
make
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/*/apps/%{name}.svg
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}/qml/*.qml
%{_datadir}/%{name}/python/*.py
%{_datadir}/%{name}/images/*.svg
%{_datadir}/%{name}/translations/*.qm
%{_datadir}/%{name}/locale/*/LC_MESSAGES/%{name}.mo

%changelog
* Mon Oct 21 2019 Yann Büchau <nobodyinperson@posteo.de> 0.2.0-jolla
- Fix algorithm list not being filled sometimes
- Calculate hash dynamically when input changes
- Add PullDownMenu to clear the input
- Add button to use digest as new input for iterative hashing
* Sun Oct 20 2019 Yann Büchau <nobodyinperson@posteo.de> 0.1.0-jolla
- basic text hashing functionality
- selectable hashing algorithm
- copying digest to clipboard
