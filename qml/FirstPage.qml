import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: firstpage

    SilicaFlickable {
        id: flickable
        anchors.fill: parent

        ViewPlaceholder {
            enabled: true
            text: qsTranslate("firstpage", "Nothing here!")
        }

    }
}


