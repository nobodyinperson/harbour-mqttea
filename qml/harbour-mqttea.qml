import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "."

ApplicationWindow
{
    id: app
    property bool loading: false
    initialPage: Component { FirstPage { } }
    cover: Qt.resolvedUrl("Cover.qml")
    Python { id: python }
}


