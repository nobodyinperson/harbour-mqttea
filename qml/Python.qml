import QtQuick 2.0
import io.thp.pyotherside 1.4

Python {
    Component.onCompleted: {
        addImportPath(Qt.resolvedUrl('..'));
        importModule('python', function () {
            console.debug("Python module imported")
        });
     }

    onError: {
        // when an exception is raised, this error handler will be called
        console.log('python error: ' + traceback);
    }

    onReceived: {
        // asychronous messages from Python arrive here
        // in Python, this can be accomplished via pyotherside.send()
        console.debug('got message from python: ' + data);
    }
}

